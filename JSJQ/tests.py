from django.test import TestCase
from django.conf import settings
from importlib import import_module
from .views import index
from django.http import HttpRequest
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve


# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class PpwUnitTest(TestCase):

    # URL TEST 
    def test_page_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_found(self):
        response = Client().get('/hayoo/')
        self.assertEqual(response.status_code, 404)

    # FUNCTION TEXT
    def test_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    # TEMPLATE USE TEST
    def test_page_uses_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Base.html')
    
    # TEXT TEST
    def test_page_has_content(self):
        request = HttpRequest()
        engine = import_module(settings.SESSION_ENGINE)
        request.session = engine.SessionStore(None)
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Pengen Tau Sesuatu?", html_response)

class CommentFunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CommentFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(CommentFunctionalTest, self).tearDown()

    def test_open(self):
        self.selenium.get(self.live_server_url + '/')
        title = self.selenium.find_element_by_id("intro")
        self.assertEqual(title.text, "Pengen Tau Sesuatu?")
